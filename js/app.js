
/* global google, moment */

/**
 * Description of app.js.js
 *
 * @author		Horatiu Cristea-Lubinschi <horatiu.cristea@gmail.com>
 * @copyright	(c) 
 * @date		Oct 24, 2017
 * @encoding	UTF-8 
 */

/**
 * @param {object} config
 * @returns {VitaminApp}
 */
var VitaminApp = function(config){
	var me = this;

	me.chartContainer = $(config.selectorChartContainer);
	me.form = $(config.selectorForm);

	me.data = null;
	me.chart = null;

	me.initForm(config);
	me.form.submit();

	function resize(){
		me.handleWindowResize();
	}
	if (window.addEventListener) {
		window.addEventListener('resize', resize);
	}
	else {
		window.attachEvent('onresize', resize);
	}	
};

VitaminApp.prototype.initForm = function(config){
	var me = this, 
		$frm = me.form,
		validator, 
		$btn;

	me.initDatasetCodesSelect(config.selectorDatasetCodes);
	me.initDatePickers();

	validator = $frm.validate({
		errorPlacement : function(error, element) {
			error.insertAfter(element.parent());
		}					
	});

	$frm.on('submit', function(e){
		e.preventDefault();
		if (!validator.valid()){
			return false; 
		}
		$btn = $frm.find('button.btn');
		$btn.button('loading');
		$.ajax({
			url: me.getDataUrl($frm.find('#dataset_code').val()), 
			data: $frm.serialize(), 
			dataType: 'json', 
			success: function(r){
				$('.alert').addClass('hidden').html('');
				me.prepareData(r.dataset_data.data);
				if (!me._chartInitialized){
					me.initChart(config);
				} else {
					me.drawChart(config);
				}
			}, 
			error: function(r){
				var json = r.responseJSON, 
					errorMsg;
				if (json && json.errors){
					validator.showErrors(json.errors);
					return;
				}
				if (json && json.quandl_error){
					errorMsg = json.quandl_error.message;
				} else {
					errorMsg = 'Unexpected error';
				}
				$('.alert').removeClass('hidden').html('<strong>Error fetching data. </strong>' + errorMsg);
			},
			complete: function(){
				$btn.button('reset');
			}
		});
	});

	me._formInitialized = true;
};

VitaminApp.prototype.initChart= function(config){
	var me = this;
	google.charts.load('current', {packages: ['corechart', 'line']});
	google.charts.setOnLoadCallback(function(){
		me.drawChart(config);
	});
	me._chartInitialized = true;
};

VitaminApp.prototype.drawChart = function(){
	var me = this; 
	me.data = new google.visualization.DataTable();
	me.data.addColumn('date', 'Days');
	me.data.addColumn('number', 'Price');
	me.data.addRows(me.preparedData);

	if (!me.chart){
		me.chart = new google.visualization.LineChart(me.chartContainer[0]);
	}
	me.chart.draw(me.data, {
		legend: 'none',
		chartArea: {
			width: '90%', 
			height: '80%'
		},
		trendlines: {0: {
			lineWidth: 15
		}}
	});	
};

VitaminApp.prototype.getDataUrl = function(datasetCode){
	return 'https://www.quandl.com/api/v3/datasets/WIKI/' + datasetCode + '/data.json'; 
};

VitaminApp.prototype.prepareData = function(rawData){
	var me = this;
	for(var i = 0; i < rawData.length; i++){
		rawData[i][0] = new Date(rawData[i][0]);
	}
	me.preparedData = rawData;
}; 

VitaminApp.prototype.handleWindowResize = function(){
	var me = this;
	if (!me._chartInitialized){
		return;
	}
	me.drawChart();
};

VitaminApp.prototype.initDatePickers = function(){
	var me = this, 
		options = {
				maxDate: moment().subtract(1, 'days'),
				showClose: true, 
				showTodayButton: true,
				showClear: true, 
				toolbarPlacement: 'top', 
				icons: {
					close: 'glyphicon glyphicon-check'
				}
			};
	$('.date-picker').datetimepicker(options);
	$('#start_date').data('DateTimePicker').date(moment().subtract(6, 'months'));
};

VitaminApp.prototype.initDatasetCodesSelect = function(selector){
	var $select = $(selector);
	$.ajax({
		url: $select.data('data-url'), 
		dataType: 'json', 
		success: function(r){
			$(selector).select2({data: r});
		}
	});
};