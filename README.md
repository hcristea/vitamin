# README #

### What is this repository for? ###

* Implementation to Vitamin Software test problem

### The Problem ###

Please implement a web page that allows a user to visualize the historical trend of end of day stock prices for available companies. 
The goal is to build a simple html page that uses a company selector as input and displays the entire historical trend for the selected company as a chart.

* Bonus #1: provide the user with a 30 days estimated future trend
* Bonus #2: provide the user with a start date end
* Bonus #3: use React

Please use the following data provider: https://www.quandl.com/docs/api?json#introduction

Expecting fully functional web page.

### The Solution ###

* Using only html and client-side javascript. A form was implemented to collect user input: (Stock code, Start & End date) 
* When the form is submited, data is fetched from QUANDL and rendered as a linechart using google chart API

